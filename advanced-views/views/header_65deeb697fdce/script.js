import {store, getContext} from '@wordpress/interactivity';

store("header", {
    actions: {
        toggle: () => {
            let context = getContext();

            context.isOpen = !context.isOpen;
        },
    }
});
