import {store, getContext, getElement} from '@wordpress/interactivity';

const COOKIE_NAME = '_cookie-consent';

const {actions} = store("gdpr-cookie-consent", {
    actions: {
        setCookie: function (name, value, expireDays) {
            let date = new Date();
            date.setTime(date.getTime() + (expireDays * 24 * 60 * 60 * 1000));
            let expires = "expires=" + date.toUTCString();
            document.cookie = name + "=" + value + ";" + expires + ";path=/";
        },
        getCookie: function (name) {
            let cookieArr = document.cookie.split(";");

            for (let i = 0; i < cookieArr.length; i++) {
                let cookiePair = cookieArr[i].split("=");

                if (name === cookiePair[0].trim()) {
                    return cookiePair[1];
                }
            }

            return null;
        },
        acceptAndHide: function () {
            actions.setCookie(COOKIE_NAME, '1', 365);

            actions.toggle(getContext());
        },
        toggle: (context) => {
            context.transform = 'translateY(110vh)' === context.transform ?
                'translateY(0)' :
                'translateY(110vh)';
        },
    },
    callbacks: {
        init: () => {
            if (null !== actions.getCookie(COOKIE_NAME)) {
                const {ref} = getElement();
                ref.remove();
                return;
            }

            let context = getContext();

            setTimeout(() => {
                actions.toggle(context);
            }, 5000);
        }
    }
});
