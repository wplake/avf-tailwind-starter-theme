const {addIconSelectors} = require('@iconify/tailwind');

/** @type {import('tailwindcss').Config} */
module.exports = {
    theme: {
        container: {
            center: true,
            padding: '1rem',
            screens: {
                xl: '1200px',
            },
        },
        fontFamily: {
            primary: '"Open Sans"',
            secondary: 'Roboto',
        },
        colors: {
            'color-action': '#b04242',
            'color-page-bg': '#202020',
            'color-element-bg': '#252728',
            'color-body-text': '#888',
            'color-heading-text': '#c6c6c6',
        },
    },
    content: {
        files: ['../advanced-views/!**!/!*.twig'],
        transform: {
            twig: (content) => {
                return content.replace(/data-wp-class--([^=]+)=["'][^'"]+['"]/g, 'class="$1"');
            }
        }
    },
    plugins: [
        addIconSelectors(['ic',]),
    ]
}