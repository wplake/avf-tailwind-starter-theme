<?php

declare( strict_types=1 );

namespace StarterTheme;

class FrontCleaner implements HooksInterface {
	// by default, ContactForm7 enqueues its assets on all the front pages.
	protected function remove_contact_form_7_front_assets_on_unrelated_pages(): void {
		add_action( 'wp_enqueue_scripts', function () {
			global $post;

			$is_related_page = true === is_a( $post, 'WP_Post' ) &&
			                   true === has_shortcode( $post->post_content, 'contact-form-7' );

			// we have custom styles, so don't need the default ones.
			wp_dequeue_style( 'contact-form-7' );

			if ( true === $is_related_page ) {
				return;
			}

			wp_dequeue_script( 'contact-form-7' );
		} );
	}

	protected function is_woo_loaded(): bool {
		return function_exists( 'is_woocommerce' );
	}

	protected function is_woo_page(): bool {
		if ( false === function_exists( 'is_woocommerce' ) ||
		     false === function_exists( 'is_cart' ) ||
		     false === function_exists( 'is_checkout' ) ||
		     false === function_exists( 'is_account_page' ) ) {
			return false;
		}

		return true === is_woocommerce() ||
		       true === is_cart() ||
		       true === is_checkout() ||
		       true === is_account_page();
	}

	// by default, Woo enqueues its assets on all the front pages.
	protected function remove_woo_front_assets_on_unrelated_pages(): void {
		add_action( 'wp_print_scripts', function () {
			if ( false === $this->is_woo_loaded() ||
			     true === $this->is_woo_page() ) {
				return;
			}

			wp_dequeue_script( 'wc-add-to-cart' );
			wp_dequeue_script( 'jquery-blockui' );
			wp_dequeue_script( 'jquery-placeholder' );
			wp_dequeue_script( 'woocommerce' );
			wp_dequeue_script( 'jquery-cookie' );
			wp_dequeue_script( 'wc-cart-fragments' );
		}, 100 );

		add_action( 'wp_enqueue_scripts', function () {
			if ( false === $this->is_woo_loaded() ||
			     true === $this->is_woo_page() ) {
				return;
			}

			wp_dequeue_style( 'woocommerce-layout' );
			wp_dequeue_style( 'woocommerce-smallscreen' );
			wp_dequeue_style( 'woocommerce-general' );
			wp_dequeue_style( 'wc-blocks-style' );
		}, 100 );
	}

	// by default, WordPress adds global variables, block styles and emojis to the front pages.
	protected function remove_wordpress_front_assets() {
		// wp version
		remove_action( 'wp_head', 'wp_generator' );
		// wp duotone
		remove_action( 'wp_body_open', 'wp_global_styles_render_svg_filters' );
		// wp emojis
		remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
		remove_action( 'wp_print_styles', 'print_emoji_styles' );
		// wp block styles
		add_action( 'wp_enqueue_scripts', function () {
			wp_dequeue_style( 'wp-block-library' );
			wp_dequeue_style( 'wp-block-library-theme' );
			wp_dequeue_style( 'storefront-gutenberg-blocks' );
			wp_dequeue_style( 'global-styles' );
		}, 100 );
	}

	public function remove_cf_turnstile_js_on_front(): void {
		add_action( 'wp_enqueue_scripts', function () {
			if ( true === is_admin() ) {
				return;
			}

			wp_dequeue_script( 'cfturnstile-woo-js' );
		} );
	}

	public function set_hooks(): void {
		$this->remove_wordpress_front_assets();
		$this->remove_woo_front_assets_on_unrelated_pages();
		$this->remove_contact_form_7_front_assets_on_unrelated_pages();
		$this->remove_cf_turnstile_js_on_front();

		// yoast
		add_filter( 'wpseo_debug_markers', '__return_false' );
		// contact-form-7
		add_filter( 'wpcf7_autop_or_not', '__return_false' );
	}
}
