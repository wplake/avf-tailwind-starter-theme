<?php

declare( strict_types=1 );

namespace StarterTheme;

interface HooksInterface {
	public function set_hooks(): void;
}
