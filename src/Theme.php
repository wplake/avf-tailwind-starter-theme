<?php

declare( strict_types=1 );

namespace StarterTheme;

class Theme implements HooksInterface {
	// when the comments feature is enabled, users can add comments via API (even without a comment form).
	protected function disable_comments(): void {
		add_filter( 'comments_open', '__return_false', 20 );
		add_filter( 'pings_open', '__return_false', 20 );
		add_action( 'admin_init', [ $this, 'remove_comments_support_from_all_post_types', ] );
	}

	protected function redirect_search_to_home(): void {
		add_action( 'template_redirect', function () {
			if ( false === is_search() ) {
				return;
			}

			wp_safe_redirect( home_url() );
			exit;
		} );
	}

	public function remove_comments_support_from_all_post_types(): void {
		$types = get_post_types();

		foreach ( $types as $type ) {
			if ( false === post_type_supports( $type, 'comments' ) ) {
				continue;
			}

			remove_post_type_support( $type, 'comments' );
			remove_post_type_support( $type, 'trackbacks' );
		}
	}

	public function set_theme_support(): void {
		add_theme_support( 'menus' );
		add_theme_support( 'custom-logo' );
		add_post_type_support( 'page', 'excerpt' );
	}

	public function set_hooks(): void {
		$this->disable_comments();
		$this->redirect_search_to_home();

		add_action( 'after_setup_theme', [ $this, 'set_theme_support' ] );
	}
}
