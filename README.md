## 1. What is it?

It's a WordPress starter theme with [Tailwind](https://tailwindcss.com/) for
the [Advanced Views Framework](https://wordpress.org/plugins/acf-views/).

### 2. About the theme

It's a [block-based theme](https://wordpress.org/documentation/article/block-themes/) with a set of pre-built generic
blocks, like header, made with Advanced Views Framework and Tailwind.

### 3. How to use

#### 3.1) LocalWP Blueprint

The simplest way to get started is to import the [LocalWP Blueprint](https://localwp.com/blueprints/),
called `localwp-blueprint.zip` inside this repository. It'll create a new site with the theme installed, along with a
set of
plugins that we use at WPLake,
like [Yoast](https://wordpress.org/plugins/wordpress-seo/), [Contact Form 7](https://wordpress.org/plugins/contact-form-7/)
and others.

Admin url: `/wp-admin`.
Admin login: `starter`.
Admin pass: `starter`.

You'll only need to change the theme namespace and name, and you're ready to go:

1. Open `vendors/composer.json` and change `StarterTheme` namespace to yours.
2. Run the composer dump command to update autoloader - `cd vendors;composer dump-autoload; cd ..;`.
3. Open the files inside the `src` folder and replace `StarterTheme` namespace with yours.
4. Open `style.css` and change the theme name, author, etc.

That's it. LocalWP is a free tool, and you can customize this installation and turn it into your starter blueprint.

#### 3.2) Manual installation

Alternatively, you can install the theme manually:

1. Make a new folder in `/wp-content/themes` and clone the repository
   inside - `git clone git@gitlab.com:wplake/avf-tailwind-starter-theme.git`.
2. Open `vendors/composer.json` and change `StarterTheme` namespace to yours.
3. Open the files inside the `src` folder and replace `StarterTheme` namespace with yours.
4. Open `style.css` and change the theme name, author, etc.
5. Install composer packages - `cd vendors;composer install; cd ..;`.
6. Install node modules - `cd tools;corepack use yarn@latest;yarn install; cd ..;`.
7. Activate the theme - visit the WordPress dashboard and activate the theme.
8. Add the `header` menu: visit Dashboard -> Appearance -> Menus and create a new menu with the 'header' name.
9. Create `Home` and `Blog` as empty pages.
10. Visit Site settings -> Reading and set the 'Front page displays' to 'A static page' and define the pages above.
11. Visit the AVF settings and enter the following code in the Sass template field inside the Defaults tab:

```scss
/* advanced-views:tailwind */
@tailwind components;
@tailwind utilities;
```

This will save you from the necessity of creating style.scss with this boilerplate manually for every View and Card.
Additionally, you can set the `Classes generation` and `Web components type` options to `none` to simplify the injection
of
Tailwind classes into the markup.

That's it! Now you can start creating pages, Views, and Cards with the Advanced Views Framework.

### 4. How to add page sections

Any section consists of two parts:

1. Data itself (fields for editors)
2. View (data representation)

So, while you create fields
using [your favorite data vendor](https://docs.acfviews.com/getting-started/supported-data-vendors) (ACF, MetaBox,
Pods), you'll create a View in the AVF to
make a presentation and define the layout.

After creating new View in the AVF, you'll add it to the target places (Gutenberg/Classic editor, sidebars, etc.).

AVF offers 2 integration ways:

1. Shortcode - you create a Fields Group (ACF/MetaBox/Pods), attach it to the page, and fill out fields. Then create a
   View for these fields and paste the shortcode to the page.
2. Custom Gutenberg Block - you create a Fields Group, then create a View and enable the Gutenberg block option. Visit
   the target page, add the new block from the Gutenberg library, and fill out the fields.

Read their
comparison [here](https://docs.acfviews.com/getting-started/introduction/key-aspects#id-2.-integration-approaches).

The Custom Gutenberg block is better for performance and also allows reusing the same field group on the same page, but
it requires the Pro version of AVF and the Pro version of your data vendor (ACF, MetaBox).

The shortcode is a free option, and while attaching meta fields to pages is a common practice, it's slower than
Gutenberg blocks because meta fields are stored in the postmeta table. It means page loading will generate a lot of DB
queries (unless you use a table storage option, but it's a paid option in most data vendors).

While you can also just hardcode the data in the View, it's not recommended as it violates the CMS idea.

### 5. How to customize

#### 5.1) Theme settings

`theme.json` is a block-theme config parsed by WordPress. Currently, it defines:

* `templateParts` - like header, footer, etc.
* `customTemplates` - templates that can be chosen in the Template dropdown in the page editor. Currently, it's `page`
  and `secondary-page`.
* `settings/layout` - defines content max-width for editors in the Gutenberg editor. Currently, it's `840px`.
* `settings/typography` - defines the fonts that will be loaded by WordPress. Currently, it's `OpenSans` and `Roboto`.

Fonts are stored in the `assets/fonts` folder. Templates available in the `templates` folder, while parts are in
the `parts` folder. It's a block theme, so both folders contain .html files, with Gutenberg blocks
using [HTML-based syntax](https://developer.wordpress.org/block-editor/getting-started/fundamentals/markup-representation-block/).

You shouldn't worry too much about these folders, as general theme templates, like header, are already pre-configured,
while page blocks will be created using Advanced Views Framework.

#### 5.2) Theme PHP code

`functions.php` is loaded by WordPress automatically. Currently, it includes the composer autoloader, which is set up to
load classes from the `src` folder.

* `Theme.php` - Disables comments and search and defines theme supports.
* `FrontCleaner.php` - Removes unnecessary scripts and styles from the front-end (WordPress, Woo, Contact Form 7, and
  CloudFlare Turnstile).

If you need comments, remove related pieces from `Theme.php`.

From the `composer.json`, you can see that it uses the [PHP-DI](https://php-di.org/) dependency injection container. You
can utilize it within
the theme and also
to [pass instances to the View and Card Custom Data](https://docs.acfviews.com/display-content/custom-data-pro#php-di-support).

#### 5.3) Advanced Views Framework

The `advanced-views` folder contains several
pre-built [Views and Cards](https://docs.acfviews.com/getting-started/introduction/key-aspects) of the Advanced Views
Framework:

Views:

* `header` - a header block.
* `footer` - a footer block.
* `gdpr` - a cookie consent block.
* `page` - block for a universal page, displays the content without extra styling.
* `secondary-page` - block for a secondary page, like `Terms and Conditions`. It limits max-width and defines
  heading/paragraph style.
* `post-preview` - block for a post preview (for the post archive).

Cards:

* `blog` - block for the posts archive.

The blocks above are already pasted into the theme `templates`:

* `index.html` - blog overview, uses the 'blog' Card
* `page.html` - default page template, uses the 'page' View
* `secondary-page.html` - secondary page template, uses the 'secondary-page' View
* `single.html` - single post template, uses the 'secondary-page' View

While `header.html` and `footer.html` in the `parts` folder use the 'header' and 'footer' Views.

If you need to define a template for your own Custom Post Type (CPT), follow these steps:
[index.html](..%2F..%2F..%2F..%2FLocal%20Sites%2Fstarter%2Fapp%2Fpublic%2Fwp-content%2Fthemes%2Fstarter%2Ftemplates%2Findex.html)

1. Create a new View for your CPT.
2. Create a `{cpt}.html` file in the `templates` folder and insert the View shortcode there (see the `page.html` file
   for reference).
3. Update the `customTemplates` section in
   the `theme.json`[index.html](..%2F..%2F..%2F..%2FLocal%20Sites%2Fstarter%2Fapp%2Fpublic%2Fwp-content%2Fthemes%2Fstarter%2Ftemplates%2Findex.html)
   file to include your new template.

#### 5.4) Tailwind

The `tools` folder contains Tailwind and [Iconify](https://iconify.design/).

Tailwind configuration is in the `tailwind.config.js` file.

Run `yarn watch` inside this folder to start watching for changes in `../advanced-views/*.twig`
and `../advanced-views/*.scss` files, which will be compiled to the related .css files.

Run `yarn build` to compile once.

Check [this repository's README.md](https://gitlab.com/wplake/tailwind-demo-for-advanced-views) for a detailed
tools explanation.

### 6. Afterwords

We use this as a starter theme at WPLake. You can customize it and create your own starter theme. We do not recommend
including commonly-used blocks (like featured section) in the starter theme.

Instead, you should use
the [Reusable Components Library](https://docs.acfviews.com/templates/reusable-components-library-pro) feature of AVF,
or import/export tools for this purpose.
