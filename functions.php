<?php

declare( strict_types=1 );

namespace StarterTheme;

use DI\Container;

require_once __DIR__ . '/vendors/vendor/autoload.php';

$atc_theme = new class implements HooksInterface {
	public function set_hooks(): void {
		$container = new Container();

		$container->get( Theme::class )->set_hooks();
		$container->get( FrontCleaner::class )->set_hooks();

		add_filter( 'acf_views/container', function () use ( $container ) {
			return $container;
		} );
	}
};

$atc_theme->set_hooks();
